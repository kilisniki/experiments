// strange eval
// node 12

// функция которую нужно выполнить через eval
const func = 'function strange(){console.log("THIS IS SO STRANGE")}'

// функция обертка
function wrapEval(func) {
  eval(func);
  strange(); // работает
  console.log(this) // WTF пустой {}
  return this;
}

// EXPERIMENT 1
// Просто вызываем функцию с оператором new чтобы получить контекст
console.log('----------------------- EXPERIMENT 1 ------------------------');
const newContext = new wrapEval(func);

console.log('1.1. Функция в возвращаемом контексте: ', newContext.strange); // undefined
//все равно пробуем вызвать
try {
  newContext.strange();
} catch (e) {
  console.log('1.2. ожидаемая ошибка', e.message);
}

console.log('1.3. Глобальный контекст: ', this)
// проверяем на всякий случай глобальный контекст
try {
  strange();
} catch (e) {
  console.log('1.4. ожидаемая ошибка', e.message);
}


// EXPERIMENT 2
// Вызываем функцию обертку через call
console.log('----------------------- EXPERIMENT 2 ------------------------');
const newContext2 = wrapEval.call({}, func);

console.log('2.1. Функция в возвращаемом контексте: ', newContext2.strange); // undefined
//все равно пробуем вызвать
try {
  newContext2.strange();
} catch (e) {
  console.log('2.2. ожидаемая ошибка', e.message);
}
console.log('2.3. Глобальный контекст: ', this)
// проверяем на всякий случай глобальный контекст
try {
  strange();
} catch (e) {
  console.log('2.4. ожидаемая ошибка', e.message);
}


// EXPERIMENT 3
// Пытаемся получить контекст с самого eval через оператор new
console.log('----------------------- EXPERIMENT 3 ------------------------');
try {
  const newContext3 = new eval(func);
} catch (e) {
  console.log('3.1 Так нельзя, потому что: ',  e.message);
}


// EXPERIMENT 4
// Пытаемся вызвать eval с подменой контекста
console.log('----------------------- EXPERIMENT 4 ------------------------');
const newContext4 = {};
eval.call(newContext4, func);

console.log('4.1. Функция в возвращаемом контексте: ', newContext4.strange); // undefined
//все равно пробуем вызвать
try {
  newContext4.strange();
} catch (e) {
  console.log('4.2. ожидаемая ошибка', e.message);
}

// проверяем на всякий случай глобальный контекст
console.log('4.3. Глобальный контекст: ', this)
try {
  strange();
  console.log('4.4. неожиданно');
} catch (e) {
  console.log('4.4? ожидаемая ошибка', e.message);
}
